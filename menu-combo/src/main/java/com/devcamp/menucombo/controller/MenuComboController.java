package com.devcamp.menucombo.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.menucombo.model.MenuCombo;
import com.devcamp.menucombo.service.MenuComboService;

@RestController
@CrossOrigin
public class MenuComboController {

    @Autowired 
    private MenuComboService menuComboService;
    @GetMapping("/menu-combo")
    public ArrayList<MenuCombo>  getAllArrayMenuComboItems(){
        return menuComboService.getAllArrayMenuCombo();
    }
    
}
