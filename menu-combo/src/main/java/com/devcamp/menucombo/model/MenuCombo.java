package com.devcamp.menucombo.model;

public class MenuCombo {
    private String size;  // size S M L
    private int duongKinh;  
    private int suongNuong;  
    private int salad;  
    private int nuocNgot;  
    private int donGia;

    // phương thức khởi tạo 
    public MenuCombo(String size, int duongKinh, int suongNuong, int salad, int nuocNgot, int donGia) {
        this.size = size;
        this.duongKinh = duongKinh;
        this.suongNuong = suongNuong;
        this.salad = salad;
        this.nuocNgot = nuocNgot;
        this.donGia = donGia;
    }
    public String getSize() {
        return size;
    }
    public void setSize(String size) {
        this.size = size;
    }
    public int getDuongKinh() {
        return duongKinh;
    }
    public void setDuongKinh(int duongKinh) {
        this.duongKinh = duongKinh;
    }
    public int getSuongNuong() {
        return suongNuong;
    }
    public void setSuongNuong(int suongNuong) {
        this.suongNuong = suongNuong;
    }
    public int getSalad() {
        return salad;
    }
    public void setSalad(int salad) {
        this.salad = salad;
    }
    public int getNuocNgot() {
        return nuocNgot;
    }
    public void setNuocNgot(int nuocNgot) {
        this.nuocNgot = nuocNgot;
    }
    public int getDonGia() {
        return donGia;
    }
    public void setDonGia(int donGia) {
        this.donGia = donGia;
    }

    

    

    
}
