package com.devcamp.menucombo.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.menucombo.model.MenuCombo;

@Service
public class MenuComboService {

    // phương thức trả về ArrayList<CMenu> với CMenu là 1 Class chứa các thuộc tính
    // của Menu

    public ArrayList<MenuCombo> getAllArrayMenuCombo() {
        // khởi tạo các đối tượng Size S , Size M , Size L
        MenuCombo sizeS = new MenuCombo("S", 20, 2, 200, 2, 150000);
        MenuCombo sizeM = new MenuCombo("M", 25, 4, 300, 3, 2000000);
        MenuCombo sizeL = new MenuCombo("L", 30, 8, 500, 4, 250000);
        // add các đối tượng trên vào Array List
        ArrayList<MenuCombo> allMenuCombo = new ArrayList<>();
        allMenuCombo.add(sizeS);
        allMenuCombo.add(sizeM);
        allMenuCombo.add(sizeL);
        return allMenuCombo;
    }
}
