package com.devcamp.menucombo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MenuComboApplication {

	public static void main(String[] args) {
		SpringApplication.run(MenuComboApplication.class, args);
	}

}
