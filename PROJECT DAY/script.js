
"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gREQUEST_STATUS_OK = 200; // GET & PUT success
const gREQUEST_READY_STATUS_FINISH_AND_OK = 4;
const gREQUEST_CREATE_SUCCESS = 201; // status 201 - tạo thành công
// lấy danh sách order
// danh sách các mã giảm giá
var gDiscountVouchers = [
  { voucherID: "A1", percentDiscount: 20 }, // mã giảm giá là A1, phần trăm giảm giá 20%
  { voucherID: "A2", percentDiscount: 10 },
  { voucherID: "A3", percentDiscount: 5 },
  { voucherID: "A4", percentDiscount: 30 },
  { voucherID: "A5", percentDiscount: 20 },
  { voucherID: "B1", percentDiscount: 25 },
  { voucherID: "B2", percentDiscount: 15 },
  { voucherID: "B3", percentDiscount: 5 },
  { voucherID: "B4", percentDiscount: 30 },
  { voucherID: "B5", percentDiscount: 40 },
];

// các combo Pizza
const gCOMBO_SMALL = "Small";
const gCOMBO_MEDIUM = "Medium";
const gCOMBO_LARGE = "Large";

// các loại pizza
const gPIZZA_TYPE_GA = "Ga";
const gPIZZA_TYPE_HAI_SAN = "HaiSan";
const gPIZZA_TYPE_BACON = "Bacon";
//đối tượng trả về khi tạo đơn 
var gCreatedOrder;
// biến lưu trữ chọn size và loại piza;
var gObjectTuyChon = {
  coPizza: "",
  loaiPizza: "",

}
var gPercent = 0; // khái báo biến để chứa phần trăm giảm giá fds
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */

/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function () {
  onPageLoading();
  $("#btn-gui-don-hang").on("click", onNavOrderDetailClick);  // gán cho nút gửi đơn hàng (orrder detail) ở nav bar
  $("#btn-small").on("click", onBtnSmallSizeClick);  // nút chọn  size S
  $("#btn-medium").on("click", onBtnMediumSizeClick);  // nút chọn  size M
  $("#btn-large").on("click", onBtnLargeSizeClick);  // nút chọn  size L
  $("#btn-ga").on("click", onBtnGaClick);  // nút chọn  pizaa OCEAN MANIA
  $("#btn-hai-san").on("click", onBtnHaiSanClick);  // nút chọn  HAWAIIAN
  $("#btn-bacon").on("click", onBtnBaconClick);  // nút chọn  HAWAIIAN
  $("#btn-modal-tao-don").on("click", taoDonClick);  // nút tạo đơn
  

});


/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// hàm load trang 
function onPageLoading() {
  console.log("đã load thẻ body");
  getListNuocUong();
   // hàm chạy phần lời chào có kèm thời gian
   getLoiChaoDateTop("thuong");
   getApiMenuCombo();   // load data lên bảng giá . có gọi api 


}
// hàm gọi gửi đơn hàng (orrder detail)
function onNavOrderDetailClick() {
  console.log("%cGọi chi tiết order ở đây", "color:blue");
  getValidateCreatedOrder(gCreatedOrder);
  // truyền id và mã order sang trang mới ,
  var vUrl = "viewOrder.html" +
    "?id=" + gCreatedOrder.id +
    "&ordercode=" + gCreatedOrder.orderCode;
  window.location.href = vUrl;
  console.log(gCreatedOrder);
 

}

// hàm được gọi khi bấm nút chọn kích cỡ S
function onBtnSmallSizeClick() {
  "use strict";
  // gọi hàm đổi màu nút
  changeComboButtonColor(gCOMBO_SMALL);
  gObjectTuyChon.coPizza = gCOMBO_SMALL;  // chọn cỡ pizza lưu trong biến toàn cục
  console.log("%c Pizza size đã chọn là : ", "color:red", gObjectTuyChon.coPizza);  // show ra console
  //tạo một đối tượng menu, được tham số hóa
  var vSelectedMenuStructure = getComboSelected("S", 20, 2, 200, 2, 150000);
  // gọi method hiển thị thông tin
  vSelectedMenuStructure.displayInConsoleLog();
}

// hàm được gọi khi bấm nút chọn kích cỡ M
function onBtnMediumSizeClick() {
  "use strict";
  // gọi hàm đổi màu nút
  changeComboButtonColor(gCOMBO_MEDIUM);  // đổi màu 
  gObjectTuyChon.coPizza = gCOMBO_MEDIUM;  // chọn cỡ pizza lưu trong biến toàn cục
  console.log("%c Pizza size đã chọn là : ", "color:red", gObjectTuyChon.coPizza);  // show ra console
  //tạo một đối tượng menu, được tham số hóa
  var vSelectedMenuStructure = getComboSelected("M", 25, 4, 300, 3, 200000);
  // gọi method hiển thị thông tin
  vSelectedMenuStructure.displayInConsoleLog();
}
// hàm được gọi khi bấm nút chọn kích cỡ L
function onBtnLargeSizeClick() {
  "use strict";
  // gọi hàm đổi màu nút
  changeComboButtonColor(gCOMBO_LARGE);
  gObjectTuyChon.coPizza = gCOMBO_LARGE;  // chọn cỡ pizza lưu trong biến toàn cục
  console.log("%c Pizza size đã chọn là : ", "color:red", gObjectTuyChon.coPizza);  // show ra console
  //tạo một đối tượng menu, được tham số hóa
  var vSelectedMenuStructure = getComboSelected("L", 30, 8, 500, 4, 250000);
  // gọi method hiển thị thông tin
  vSelectedMenuStructure.displayInConsoleLog();
}
//...................[[chọn kiểu pizza]]...............................................................................
// hàm được gọi khi bấm nút chọn loại pizza gà
function onBtnGaClick() {
  "use strict";
  // gọi hàm đổi màu nút
  changeTypeButtonColor(gPIZZA_TYPE_GA);
  gObjectTuyChon.loaiPizza = gPIZZA_TYPE_GA;   // chọn cỡ pizza lưu trong biến toàn cục
  console.log("%c Pizza đã chọn là : ", "color:red", gObjectTuyChon.loaiPizza);  // show ra console
}
// hàm được gọi khi bấm nút chọn loại pizza Hải sản
function onBtnHaiSanClick() {
  "use strict";
  // gọi hàm đổi màu nút
  changeTypeButtonColor(gPIZZA_TYPE_HAI_SAN);
  gObjectTuyChon.loaiPizza = gPIZZA_TYPE_HAI_SAN;   // chọn cỡ pizza lưu trong biến toàn cục
  console.log("%c Pizza đã chọn là : ", "color:red", gObjectTuyChon.loaiPizza);  // show ra console
}
// hàm được gọi khi bấm nút chọn loại pizza Hun khói
function onBtnBaconClick() {
  "use strict";
  // gọi hàm đổi màu nút
  changeTypeButtonColor(gPIZZA_TYPE_BACON);
  gObjectTuyChon.loaiPizza = gPIZZA_TYPE_BACON;   // chọn cỡ pizza lưu trong biến toàn cục
  console.log("%c Pizza đã chọn là : ", "color:red", gObjectTuyChon.loaiPizza);  // show ra console
}
// Hàm được gọi khi click nút kiểm tra đơn
function onBtnKiemTraDonClick() {
  console.log("%c Kiểm tra đơn", "color: orange;");
  // Bước 1: Đọc
  var vOrder = getOrder();
  console.log("nút kiểm tra đơn", vOrder);
  //Bước 2: kiểm tra
  var vKetQuaKiemTra = checkValidatedForm(vOrder);
  if (vKetQuaKiemTra) {
    //Bước 3: Hiển thị
    $("#exampleModalCenter").modal("show");
    showOrderInfo(vOrder);

  }
}
//  hàm tạo đơn .. ở trên modal
function taoDonClick(){
  $("#exampleModalCenter").modal("hide");
 
  var vOrder = getOrder();
  console.log("order", vOrder);
  inThongTinRaConsole(vOrder);
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//hàm hiển thị thông tin combo từ Api
function getApiMenuCombo(){
  $.ajax({
    type: "GET",
    url: "http://localhost:8088/cmenu/menu-combo",
    data: "data",
    dataType: "json",
    success: function (response) {
      // load data lên font end
      getLoadDataInMenu(response);

    },
    error: function(error) {
      console.log(error);
    }
      
  });
}

function getLoadDataInMenu(response) {
  console.log(response[1]);
  $('.duong-kinh-cb').each(function(index) {
    $(this).text( (response[index].duongKinh) + " cm");
  });

  $('.suon-nuong-cb').each(function(index) {
    $(this).text( (response[index].suongNuong));
  });

  $('.salad-cb').each(function(index) {
    $(this).text( (response[index].salad) + " gr");
  });

  $('.nuoc-ngot-cb').each(function(index) {
    $(this).text( (response[index].nuocNgot));
  });

  $('.don-gia-cb').each(function(index) {
    $(this).text( (response[index].donGia));
  });

}


// hàm chọn combo 
function getComboSelected(
  paramMenuName,
  paramDuongKinhCM,
  paramSuongNuong,
  paramSaladGr,
  paramDrink,
  paramPriceVND
) {
  var vSelectedMenu = {
    menuName: paramMenuName, // S, M, L
    duongKinhCM: paramDuongKinhCM,
    suongNuong: paramSuongNuong,
    saladGr: paramSaladGr,
    drink: paramDrink,
    priceVND: paramPriceVND,
    displayInConsoleLog() {
      console.log("%cPLAN MENU COMBO - .........", "color:blue");
      console.log(this.menuName); //this = "đối tượng này"
      console.log("duongKinhCM: " + this.duongKinhCM);
      console.log("suongNuong: " + this.suongNuong);
      console.log("saladGr: " + this.saladGr);
      console.log("drink:" + this.drink);
      console.log("priceVND: " + this.priceVND);
    },
  };
  return vSelectedMenu;
}

// hàm thu thập(đọc) thông tin khách hàng
function getOrder() {

  var vSelectedMenuStructure = getComboSelected("", 0, 0, 0, 0, 0);  // khai báo 
  if (gObjectTuyChon.coPizza === gCOMBO_SMALL) {
    vSelectedMenuStructure = getComboSelected("S", 20, 2, 200, 2, 150000);
  }
  else if (gObjectTuyChon.coPizza === gCOMBO_MEDIUM) {
    vSelectedMenuStructure = getComboSelected("M", 25, 4, 300, 3, 200000);
  }
  else if (gObjectTuyChon.coPizza === gCOMBO_LARGE) {
    vSelectedMenuStructure = getComboSelected("L", 30, 8, 500, 4, 250000);
  }
  // thu thập dữ liệu 
  var vValueInputName = $("#inp-name").val().trim();   // họ tên
  var vValueInputEmail = $("#inp-email").val().trim();  // email
  var vValueInputPhone = $("#inp-phone").val().trim();  // số điện thoại
  var vValueInputAddress = $("#inp-address").val().trim();  // địa chỉ 
  var vValueInputMessage = $("#inp-message").val().trim();  // lời nhắn
  var vValueInputVoucherID = $("#inp-voucher").val().trim();  // mã giảm giá
  var vMaNuocUong = $("#select-nuoc-uong").val().trim();  // loại nước uống
  // khai báo đối tượng để lưu voucher 
  var vObjectGiamGia = {
    phanTramGiamGia: 0,
  }
  // hàm trả về phần trăm giảm giá
  if (vValueInputVoucherID != "") {
    getMaGiamGiaSever(vValueInputVoucherID, function (data) {
      console.log("log từ api", data.phanTramGiamGia);
      vObjectGiamGia.phanTramGiamGia = data.phanTramGiamGia;
    })
  }
  // gán các giá trị cho các trường dữ liệu  95531
  var vOrderInfo = {
    menuCombo: vSelectedMenuStructure,
    loaiPizza: gObjectTuyChon.loaiPizza,
    loaiNuocUong: vMaNuocUong,
    hoVaTen: vValueInputName,
    email: vValueInputEmail,
    dienThoai: vValueInputPhone,
    diaChi: vValueInputAddress,
    loiNhan: vValueInputMessage,
    voucher: vValueInputVoucherID,
    phanTramGiamGia: vObjectGiamGia.phanTramGiamGia,
    priceAnnualVND: function () {
      var vTotal = this.menuCombo.priceVND * (1 - this.phanTramGiamGia / 100);
      return vTotal;
    }
  };
  return vOrderInfo;
}
// ham kiem tra thong tin dat hang
// input: doi tuong khach hang
// output: dung/ sai (true/ false)
function checkValidatedForm(paramOrder) {
  if (paramOrder.menuCombo.menuName === "") {
    alert("Chọn combo pizza...");
    return false;
  }
  if (paramOrder.loaiPizza === "") {
    alert("Chọn loại pizza...");
    return false;
  }
  if (paramOrder.loaiNuocUong === "0") {
    alert("Chọn loại nước uống...");
    return false;
  }
  if (paramOrder.hoVaTen === "") {
    alert("Nhập họ và tên");
    return false;
  }
  if (isEmail(paramOrder.email) == false) {
    return false;
  }

  if (paramOrder.dienThoai === "") {
    alert("Nhập vào số điện thoại...");
    return false;
  }
  if (isNaN(paramOrder.dienThoai)) {
    alert("số điện thoại  là số nguyên...");
    return false;
  }
  if (paramOrder.diaChi === "") {
    alert("Địa chỉ không để trống...");
    return false;
  }
  return true;
}

//hàm kiểm tra email
function isEmail(paramEmail) {
  if (paramEmail < 3) {
    alert("Nhập email...");
    return false;
  }
  if (paramEmail.indexOf("@") === -1) {
    alert("Email phải có ký tự @");
    return false;
  }
  if (paramEmail.startsWith("@") === true) {
    alert("Email không bắt đầu bằng @");
    return false;
  }
  if (paramEmail.endsWith("@") === true) {
    alert("Email kết thúc bằng @");
    return false;
  }
  return true;
}

// hàm đổi mầu nút khi chọn combo
function changeComboButtonColor(paramPlan) {
  var vBtnBasic = $("#btn-small"); // truy vấn nút chọn kích cỡ small
  var vBtnMedium = $("#btn-medium"); //truy vấn nút chọn kích cỡ medium
  var vBtnLarge = $("#btn-large"); //truy vấn nút chọn kích cỡ large
  if (paramPlan === gCOMBO_SMALL) {
    //nếu chọn menu Small thì thay màu nút chọn kích cỡ small bằng màu cam (btn-warning), hai nút kia xanh (btn-success)
    vBtnBasic.attr("class", "btn btn-success");
    vBtnMedium.attr("class", "btn btn-warning");
    vBtnLarge.attr("class", "btn btn-warning");
  } else if (paramPlan === gCOMBO_MEDIUM) {
    //nếu chọn menu Medium thì thay màu nút chọn kích cỡ Medium bằng màu cam (btn-warning), hai nút kia xanh (btn-success)
    vBtnBasic.attr("class", "btn btn-warning");
    vBtnMedium.attr("class", "btn btn-success");
    vBtnLarge.attr("class", "btn btn-warning");
  } else if (paramPlan === gCOMBO_LARGE) {
    //nếu chọn menu Large thì thay màu nút chọn kích cỡ Large bằng màu cam (btn-warning), hai nút kia xanh (btn-success)
    vBtnBasic.attr("class", "btn btn-warning");
    vBtnMedium.attr("class", "btn btn-warning");
    vBtnLarge.attr("class", "btn btn-success");
  }
}

// hàm đổi mầu nút khi chọn loại pizza
function changeTypeButtonColor(paramType) {
  var vBtnGa = $("#btn-ga"); // truy vấn nút chọn loại pizza gà
  var vBtnHaiSan = $("#btn-hai-san"); //truy vấn nút chọn loại pizza hải sản
  var vBtnBacon = $("#btn-bacon"); //truy vấn nút chọn loại pizza bacon
  if (paramType === gPIZZA_TYPE_GA) {
    //nếu chọn loại pizza Gà thì thay màu nút chọn loại pizza gà bằng màu cam (btn-warning), hai nút kia xanh (btn-success)
    vBtnGa.attr("class", "btn btn-success w-100");
    vBtnHaiSan.attr("class", "btn btn-warning w-100");
    vBtnBacon.attr("class", "btn btn-warning w-100");
  } else if (paramType === gPIZZA_TYPE_HAI_SAN) {
    //nếu chọn loại pizza Hải sản thì thay màu nút chọn loại pizza Hải sản bằng màu cam (btn-warning), hai nút kia xanh (btn-success)
    vBtnGa.attr("class", "btn btn-warning w-100");
    vBtnHaiSan.attr("class", "btn btn-success w-100");
    vBtnBacon.attr("class", "btn btn-warning w-100");
  } else if (paramType === gPIZZA_TYPE_BACON) {
    //nếu chọn loại pizza Bacon thì thay màu nút chọn loại pizza Bacon bằng màu cam (btn-warning), hai nút kia xanh (btn-success)
    vBtnGa.attr("class", "btn btn-warning w-100");
    vBtnHaiSan.attr("class", "btn btn-warning w-100");
    vBtnBacon.attr("class", "btn btn-success w-100");
  }
}
// goi api để lấy dữ liệu nước uống 
function getListNuocUong() {
  "use strict";
  // gọi sever và list đồ uống ra ô select
  $.ajax({
    type: "GET",
    url: "http://203.171.20.210:8080/devcamp-pizza365/drinks",
    dataType: "json",
    success: function (data) {
      console.log("list nuoc uong", data);
      for (var i = 0; i < data.length; i++) {
        var vElemet = data[i];
        var vRowOption = $("<option>").val(vElemet.maNuocUong).html(vElemet.tenNuocUong);
        $("#select-nuoc-uong").append(vRowOption);
      }
    },
    error: function (error) {
      console.log("đã xảy ra lỗi khi gọi server list nước uống : ", error.status)
    }
  });
}


/// hàm show thông tin đặt hàng
function showOrderInfo(paramOrder) {
  $("#ipn-modal-ho-ten").val(paramOrder.hoVaTen);
  $("#ipn-modal-so-dien-thoai").val(paramOrder.dienThoai);
  $("#ipn-modal-dia-chi").val(paramOrder.diaChi);
  $("#ipn-modal-loi-nhan").val(paramOrder.loiNhan);
  $("#ipn-modal-ma-giam-gia").val(paramOrder.voucher);
  // xây dựng nội dung cho ô thông tin chi tiết
  var vText = "Xác nhận: " + paramOrder.hoVaTen + ", Số điện thoại" + paramOrder.dienThoai + ", Địa chỉ" + paramOrder.diaChi 
  + "\n" + 
  "Menu" + paramOrder.menuCombo.menuName + ", Sườn nướng" + paramOrder.menuCombo.suongNuong + ", nước" +
  paramOrder.menuCombo.drink + "...."  + "\n" +
  "Loại Pizza: " + paramOrder.loaiPizza + ", Giá " + paramOrder.menuCombo.priceVND + " Mã giảm giá: " + paramOrder.voucher 
  + " Phải thanh toán:  " + paramOrder.priceAnnualVND()  + " (Giảm giá  " + paramOrder.phanTramGiamGia + " % )";
  
  $("#ipn-modal-thong-tin-chi-tiet").val(vText);
}
// kiểm tra mã giảm giá từ sever 
// input mã giảm giá (sring);
// output . phần trăm giảm giá . (number)
function getMaGiamGiaSever(pamamMaGiamGia, paramFunction) {
  "use strict";
  //một số mã đúng để test: 95531, 81432,...lưu ý test cả mã sai
  $.ajax({
    type: "GET",
    url: "http://203.171.20.210:8080/devcamp-pizza365/voucher_detail/" + pamamMaGiamGia,
    data: "data",
    dataType: "json",
    async: false,
    success: paramFunction,
    error: function () {
     console.log("gọi server lấy voucher không thành công ");
    }
  });
}

// Hàm ghi thông tin đặt hàng ra console và hiển thị lên wed
function inThongTinRaConsole(paramOrder) {
  console.log("%c Thông tin đặt hàng", "color: green;");
  console.log("Họ và tên: " + paramOrder.hoVaTen);
  console.log("Email: " + paramOrder.email);
  console.log("Địa chỉ: " + paramOrder.diaChi);
  console.log("Lời nhắn: " + paramOrder.loiNhan);
  console.log(".....................................");
  console.log("Kích cỡ: " + paramOrder.menuCombo.menuName);
  console.log("Đường kính: " + paramOrder.menuCombo.duongKinhCM);
  console.log("Sườn nướng: " + paramOrder.menuCombo.suongNuong);
  console.log("Salad : " + paramOrder.menuCombo.saladGr);
  console.log("Nước Ngọt : " + paramOrder.menuCombo.drink);
  console.log(".....................................");
  console.log("Loại pizza : " + paramOrder.loaiPizza);
  console.log("Mã voucher : " + paramOrder.voucher);
  console.log("Giá vnd : " + paramOrder.menuCombo.priceVND);
  console.log("Discount %: " + paramOrder.phanTramGiamGia);
  console.log("Phải thanh toán vnd: " + paramOrder.priceAnnualVND());

  const vBASE_URL = "http://203.171.20.210:8080/devcamp-pizza365/orders";
  // khai báo object order để chứa thông tin đặt hàng với 14 thuộc tính
  var vObjectRequest = {
    kichCo: paramOrder.menuCombo.menuName,
    duongKinh: paramOrder.menuCombo.duongKinhCM,
    suon: paramOrder.menuCombo.suongNuong,
    salad: paramOrder.menuCombo.saladGr,
    loaiPizza: paramOrder.loaiPizza,
    idVourcher: paramOrder.voucher,
    idLoaiNuocUong: paramOrder.loaiNuocUong,
    soLuongNuoc: paramOrder.menuCombo.drink,
    hoTen: paramOrder.hoVaTen,
    thanhTien: paramOrder.menuCombo.priceVND,  // giá 
    email: paramOrder.email,
    soDienThoai: paramOrder.dienThoai,
    diaChi: paramOrder.diaChi,
    loiNhan: paramOrder.loiNhan
  }

    $.ajax({
      type: "POST",
      contentType: "application/json;charset=UTF-8",
      url: vBASE_URL,
      data: JSON.stringify(vObjectRequest),
      dataType: "json",
      success: function (data) {
        console.log("ajax object",data);
        $("#ma-order").val(data.orderCode);  //  gán mã code vào ô dữ liệu trên modal 
        $("#exampleModal2").modal("show");
      }
    });

    }
function getValidateCreatedOrder(paramObject) {
  if (paramObject == null) {
    alert("không có id và Ordercode");
    return false;
  }
  return true;
}

function getLoiChaoDateTop(paramName){
  console.log("thumbnail");
  $.ajax({
    type: "GET",
    url: "http://localhost:8088/daily-campaign/devcamp-date?username=" + paramName,
    dataType: "text",
    data: "data",
    success: function (response) {
      $("#loi-chao").html(response);
    },
    error: function (response) {
      console.log(response);
    }
  });
}


